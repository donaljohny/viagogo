import helper as hp
import random
from Location import Location
from Event import Event
from Ticket import Ticket


class World:
    def __init__(self):
        self.locations_with_events = []
        for i in range(1, hp.MAX_EVENT_SIZE+1):
            while True:
                x_cord = float('{:,.2f}'.format(random.uniform(-10.0, 10.0)))
                y_cord = float('{:,.2f}'.format(random.uniform(-10.0, 10.0)))
                location = Location(x_cord, y_cord)

                if location.can_add_event():
                    tickets = []
                    event = Event(i, tickets)

                    for j in range(0, random.randint(0, hp.MAX_TICKETS)):
                        ticket_price = hp.MAX_TICKET_PRIZE * (1.0 - random.random())
                        ticket = Ticket(j, ticket_price)
                        event.add_ticket(ticket)

                    location.add_event(event)
                    self.locations_with_events.append(location)
                    break
