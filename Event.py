

class Event:
    def __init__(self, event_id, tickets=[]):
        """

        :param id:
        :param tickets:
        :return:
        """
        self.event_id = event_id
        self.tickets = tickets
        self.cheapest_ticket = None

    def set_cheapest_ticket(self, ticket):
        """

        :param ticket:
        :return:
        """
        self.cheapest_ticket = ticket

    def get_cheapest_ticket(self):
        """

        :return:
        """
        return self.cheapest_ticket

    def add_ticket(self, ticket):
        """

        :param ticket:
        :return:
        """
        self.tickets.append(ticket)
        if self.get_cheapest_ticket()is None or ticket.price <= self.cheapest_ticket.price:
            self.set_cheapest_ticket(ticket)

    def __str__(self):
        if self.cheapest_ticket is None:
            str_ = "Event id: " + str(self.event_id) + '-' + "No tickets available for this event"
        else:
            str_ = "Event id: " + str(self.event_id) + '-' + str(self.cheapest_ticket)
        return str_
