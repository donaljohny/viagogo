<center><h1>viagogo coding test.</h1></center>

# Scenario
* Your program should randomly generate seed data.
* Your program should operate in a world that ranges from -10 to +10 (Y axis), and -10
to +10 (X axis).
* Your program should assume that each co-ordinate can hold a maximum of one
event.
* Each event has a unique numeric identifier (e.g. 1, 2, 3).
* Each event has zero or more tickets.
* Each ticket has a non-zero price, expressed in USDollars.
* The distance between two points should be computed as the Manhattandistance.
Instructions

* You are required to write a program which accepts a user location as a pair of co-
ordinates, and returns a list of the five closest events, along with the cheapest ticket price for each event.
---
# Questions
* How might you change your program if you needed to support multiple events at the
same location?

The program already supports this.
* How would you change your program if you were working with a much larger world
size?

Instead of loding world information into the memory, I'd use a database. 

# Usage
* Run `python Viagogo_Solution.py`
* The program will ask for the co-ordinates, pass them seperated by comma. 
# example output. 

  ```
  Please input co-ordinates: 4, 2
  Event id: 54-$0.84, Distance 1.8
  Event id: 71-$7.79, Distance 0.5
  Event id: 74-$19.17, Distance 0.5
  ```
