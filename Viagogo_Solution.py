import helper as hp
from Location import Location
from World import World


def run(world):
    """

    :param world:
    :return:
    """
    try:
        coordinates = input("Please input co-ordinates: ").split(',')

        x_cord = float(coordinates[0])
        y_cord = float(coordinates[1])

        hp.check_input(x_cord, y_cord)
    except (ValueError, IndexError):
        print ("please enter proper co-ordinates")

    user_input = Location(x_cord, y_cord)
    closest_event = [(None, float('inf'))] * hp.MAX_CLOSE_EVENTS
    for event in world.locations_with_events:
        distance = hp.calc_manhattan_distance(event, user_input)
        max_dis = max(closest_event, key=lambda x: x[1])
        if distance <= max_dis[1]:
            closest_event.remove(max_dis)
            closest_event.append((event, distance))
    for (location, distance) in closest_event:
        for event in location.events:
            print (str(event)+', Distance {:,.1f}'.format(distance))


def main():
    world = World()
    run(world)


if __name__ == "__main__":
    main()
