
class Ticket:
    def __init__(self, event_id, price):
        """

        :param event_id:
        :param price:
        :return:
        """
        self.event_id = event_id
        self.price = price

    def __str__(self):
        return '${:,.2f}'.format(self.price)