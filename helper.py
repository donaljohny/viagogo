import sys

"""
Contains const variables and helper methods.
"""

MAX_HEIGHT = 10
MAX_WIDTH = 10
MAX_EVENT_SIZE = 100
MAX_TICKET_PRIZE = 400
MAX_TICKETS = 100
MAX_CLOSE_EVENTS = 3
EVENT_PER_LOCATION = 1


def calc_manhattan_distance(location1, location2):
    """
    Method for calculating the manhattan distance
    :param location1:
    :param location2:
    :return:
    """

    return abs(location1.x - location2.x) + abs(location1.y - location2.y)


def check_input(x, y):
    if not -MAX_WIDTH <= x <= MAX_WIDTH or not -MAX_HEIGHT <= y <= MAX_HEIGHT:
        print ("Please enter a valid co-ordinate, between 10 and -10")
        sys.exit(0)
