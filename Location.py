import helper


class Location:
    def __init__(self, x, y):
        self.x = x
        self.y = y
        self.events = []  # list to hold events, we start with empty one.

    def can_add_event(self):
        """
        we want to make sure we are able to add an event to a given location
        so we check that here.
        :return: bool
        """
        return len(self.events) < helper.EVENT_PER_LOCATION

    def add_event(self, event):
        """
        Add a given event to the event list
        :param event:
        :return:
        """
        self.events.append(event)

    def __str__(self):
        return '('+str(self.x)+','+str(self.y)+')'
